CREATE TABLE IF NOT EXISTS results (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    name varchar(255) NOT NULL,
    app_name varchar(255) NOT NULL,
    icon_path varchar(2048) NOT NULL,
    path varchar(2048) NOT NULL,
    generic varchar(255) NOT NULL,
    cat_name varchar(255) NULL,
    sub_cat_name varchar(255) NULL,
    sub_sub_cat_name varchar(255) NULL,
    in_mini BOOLEAN NOT NULL CHECK (in_mini IN (0, 1)),
    in_super BOOLEAN NOT NULL CHECK (in_super IN (0, 1)),
    in_maxi BOOLEAN NOT NULL CHECK (in_maxi IN (0, 1)),
    in_prof BOOLEAN NOT NULL CHECK (in_prof IN (0, 1))
);

CREATE TABLE IF NOT EXISTS keywords (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    text varchar(255) NOT NULL
);

CREATE TABLE IF NOT EXISTS contains (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    id_results INTEGER,
    id_keywords INTEGER,
    FOREIGN KEY(id_results) REFERENCES results(id)
    FOREIGN KEY(id_keywords) REFERENCES keywords(id)
);
