#!/usr/bin/python3

import os
import io
import unicodedata

from shutil import copyfile
from subprocess import Popen, PIPE

from PIL import Image
import yaml

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk

from pstore.lib.config import Conf
from pstore.lib.request import DataBase
from pstore.lib.desktop import get_info


def get_desktop_path(app_name, name, cmd, real_cmd):
    cmd = real_cmd[real_cmd.rfind('/') + 1:]
    real_cmd = real_cmd[real_cmd.rfind('/') + 1:]
    if not app_name:
        return ''
    p = Popen(
        "dpkg-query -L %s | grep .desktop$" % app_name,
        shell = True,
        stdout = PIPE
    )
    desktops = p.stdout.readlines()
    if len(desktops) == 1:
        return desktops[0].decode("utf-8").strip()
    for desktop_path in desktops:
        desktop_path = desktop_path.decode("utf-8").strip()
        desktop_info = get_info(desktop_path)
        if not desktop_info:
            continue
        if desktop_info[1] in [app_name, name, cmd, real_cmd]:
            return desktop_path


def get_app_keys(app_name, name, cmd, real_cmd):
    desktop_path = get_desktop_path(app_name, name, cmd, real_cmd)
    if not desktop_path:
        return [app_name, name]
    return get_info(desktop_path)[4] + [app_name, name]


def get_apt_name(real_cmd):
    if not real_cmd:
        return ''
    p = Popen(
        "dpkg -S %s" % real_cmd,
        shell = True,
        stdout = PIPE
    )
    results = p.stdout.readlines()
    if len(results) == 0:
        return ''
    result = results[0].decode("utf-8").strip()
    return result[:result.find(':')].lower()


def get_real_cmd(cmd):
    p = Popen(
        "which %s" % cmd,
        shell = True,
        stdout = PIPE
    )
    results = p.stdout.readlines()
    if len(results) == 0:
        return ''
    return results[0].decode("utf-8").strip()

def mv_icon(icon_path, name):
    if not icon_path:
        return ''
    icon_name = unicodedata.normalize('NFD', name) \
        .replace('\'', ' ') \
        .replace(' ', '-') \
        .replace('--', '-') \
        .replace('--', '-') \
        .replace('--', '-') \
        .encode('ascii', 'ignore') \
        .decode('ascii') \
        .lower()
    if icon_path.endswith('.svg'):
        new_path = '/assets/%s.svg' % icon_name
        copyfile(icon_path, new_path)
        return new_path
    new_path = '/assets/%s.png' % icon_name
    try:
        im = Image.open(icon_path)
        im.thumbnail((48, 48))
        rgb_im = im.convert('RGBA')
        rgb_im.save(new_path, "PNG")
    except:
        return ''
    return new_path


def import_app(sessions, db, name, icon_path, cmd, generic, session):
    icon_theme = Gtk.IconTheme.get_default()
    icon_gtk = icon_theme.lookup_icon(icon_path, 48, 0)
    if icon_gtk:
        icon_path = icon_gtk.get_filename()
    elif not os.path.exists(icon_path):
        icon_path = ''
    app_exist = db.app_exist(name, sessions, session)
    if app_exist:
        if app_exist[session]:
            return
        db.update_app(name, session)
        return
    real_cmd = get_real_cmd(cmd)
    app_name = get_apt_name(real_cmd)
    new_path = mv_icon(icon_path, name)
    id_results = db.add_app(
        sessions,
        app_name,
        name,
        new_path,
        cmd,
        generic,
        session
    )
    keywords = get_app_keys(app_name, name, cmd, real_cmd)
    for keyword in keywords:
        if keyword == '':
            continue
        db.add_keyword(keyword.lower(), id_results)

def import_apps(sessions, db, apps, session):
    for a in apps:
        import_app(
            sessions,
            db,
            a['name'],
            a['icon'],
            a['cmd'],
            a['generic'],
            'in_%s' % session
        )


def import_session(sessions, db, path, session):
    if not os.path.isfile(path):
        return
    with io.open(path, 'r') as stream:
        try:
            config = yaml.load(stream)
        except:
            print('Soucis import sur %s' % path)
            return
    for s in config:
        n = len(s['apps'])
        if n == 0:
            continue
        import_apps(sessions, db, s['apps'], session)


if __name__ == "__main__":
    os.makedirs('./assets', exist_ok=True)
    conf = Conf()
    db = DataBase(conf)

    for session in conf.sessions:
        sessions = conf.sessions.copy()
        sessions.remove(session)
        import_session(
            sessions,
            db,
            '/etc/handymenu/handymenu-{}.yaml'.format(session),
            session
        )
        import_session(
            sessions,
            db,
            '/etc/handymenu/handymenu-{}.default.yaml'.format(session),
            session
        )
