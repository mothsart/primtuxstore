.PHONY: unittest flake8 test diagram clean

unittest:
	python3 -m unittest

flake8:
	flake8 --filename="*.py./primtuxmenu"

test:
	make unittest
	make flake8

diagram:
	dot -Tpng diagram.dot -o diagram.png
	dot -Tsvg diagram.dot -o diagram.svg

clean:
	rm -f MANIFEST
	rm -rf build dist
