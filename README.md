# Primtux Store

## Installation

Nécessite le paquet "python-apt"

## Utilisation developpeur :

```sh
git clone https://framagit.org/mothsart/primtuxstore
cd primtuxstore
gksu ./primtuxstore
```

## Tests

```sh
python3 -m unittest pstore/tests/tests.py
```

## Create a Debian package

```sh
git clone https://framagit.org/mothsart/primtuxstore
cd primtuxstore
dpkg-buildpackage -us -uc
```

and launch with :

```sh
sudo dpkg -i ../primtuxstore*_all.deb
```
