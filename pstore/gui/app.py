import os
import sys
import math

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gdk, GdkPixbuf

def open_app(button, cmd):
    os.system('%s &' % cmd)

def install_app(install_widget, pkg, cache, test_widget):
    pkg.mark_install()
    cache.commit()
    test_widget.set_sensitive(True)
    install_widget.set_sensitive(False)

class FakePkg:
    is_installed = False

def add_soft(soft, vbox, cache):
    pkg = FakePkg()
    if soft['app_name'] in cache.keys():
        pkg = cache[soft['app_name']]

    hbox = Gtk.HBox(homogeneous=True, margin=5)
    hbox.set_name('apps')

    # image
    if 'icon' in soft:
        if os.path.exists(soft['icon']):
            img = Gtk.Image()
            pixbuf = GdkPixbuf.Pixbuf.new_from_file(soft['icon'])
            img.set_from_pixbuf(pixbuf)
            hbox.pack_start(img, True, False, False)

    # name
    label = Gtk.Label(soft['name'], margin=5)
    hbox.pack_start(label, True, False, False)

    # Test
    test_button = Gtk.Button.new_with_label("Tester")
    enable = 'name' in soft and pkg.is_installed
    test_button.set_sensitive(enable)
    if (enable):
        test_button.connect("clicked", open_app, soft['cmd'])

    # Install
    if pkg.is_installed:
        is_installed_icon = Gtk.Image()
        is_installed_icon.set_from_stock(
            Gtk.STOCK_YES,
            Gtk.IconSize.MENU
        )
        hbox.pack_start(is_installed_icon, False, False, False)
    else:
        install_button = Gtk.Button.new_with_label("Installer")
        if isinstance(pkg, FakePkg):
            install_button.set_sensitive(False)
        else:
            install_button.connect(
                'clicked',
                install_app,
                pkg,
                cache,
                test_button
            )
        hbox.pack_start(install_button, False, False, False)

    hbox.pack_start(test_button, False, False, False)

    # Checks in handymenu
    in_menu_vbox = Gtk.VBox(homogeneous=False)
    in_label = Gtk.Label('Intégrer au :')
    in_menu_vbox.pack_start(in_label, True, False, False)
    
    in_mini_hbox = Gtk.HBox(homogeneous=False)
    in_mini = False
    if 'in_mini' in soft:
        in_mini = bool(soft['in_mini'])
    in_mini_label = Gtk.Label('handymenu-mini', margin=5)
    in_mini_button = Gtk.CheckButton(margin=5)
    in_mini_button.set_active(in_mini)
    in_mini_hbox.pack_start(in_mini_label, True, False, False)
    in_mini_hbox.pack_start(in_mini_button, True, False, False)
    in_menu_vbox.pack_start(in_mini_hbox, False, False, False)

    in_super_hbox = Gtk.HBox(homogeneous=False)
    in_super = False
    if 'in_super' in soft:
        in_super = bool(soft['in_super'])
    in_super_label = Gtk.Label('handymenu-super', margin=5)
    in_super_button = Gtk.CheckButton(margin=5)
    in_super_button.set_active(in_super)
    in_super_hbox.pack_start(in_super_label, True, False, False)
    in_super_hbox.pack_start(in_super_button, True, False, False)
    in_menu_vbox.pack_start(in_super_hbox, False, False, False)

    in_maxi_hbox = Gtk.HBox(homogeneous=False)
    in_maxi = False
    if 'in_maxi' in soft:
        in_maxi = bool(soft['in_maxi'])
    in_maxi_label = Gtk.Label('handymenu-maxi', margin=5)
    in_maxi_button = Gtk.CheckButton(margin=5)
    in_maxi_button.set_active(in_maxi)
    in_maxi_hbox.pack_start(in_maxi_label, True, False, False)
    in_maxi_hbox.pack_start(in_maxi_button, True, False, False)
    in_menu_vbox.pack_start(in_maxi_hbox, False, False, False)

    hbox.pack_start(in_menu_vbox, True, False, False)
    vbox.pack_start(hbox, False, False, False)

def update_softs(
    page_b,
    db,
    vbox,
    app_box,
    cache,
    results_by_page,
    page,
    pagination_widget,
    value=None
):
    if value and value.strip() == '':
        value = None
    if app_box.get_children() != []:
        app_box.remove(app_box.get_children()[0])
    app_vbox = Gtk.VBox(homogeneous = False)
    app_vbox.set_size_request(1024, 800)

    nb_results = db.count_results(value)
    if nb_results == 0:
        return
    nb_pages = math.ceil(nb_results / results_by_page)
    softs = db.get_page(
        (page - 1) * results_by_page,
        results_by_page,
        value
    )
    if not softs:
        return
    for soft in softs:
        soft_dic = {
            'name': soft[0],
            'app_name': soft[1],
            'generic': soft[2],
            'icon': soft[3],
            'cmd': soft[4],
            'in_mini': soft[5],
            'in_maxi': soft[6],
            'in_super': soft[7],
            'in_prof': soft[8]
        }
        add_soft(soft_dic, app_vbox, cache)
    app_box.pack_start(app_vbox, False, False, False)

    if len(vbox.get_children()) > 2:
        vbox.remove(vbox.get_children()[2])
    vbox.pack_start(
        pagination_widget(
            db,
            vbox,
            app_box,
            cache,
            page,
            results_by_page,
            nb_pages,
            update_softs,
            value
        ),
        False, False, False
    )
    app_box.show_all()
