import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gdk

from ..gui.app import update_softs

def autocomple_results(
    db,
    vbox,
    app_box,
    cache,
    results_by_page,
    page,
    pagination_widget,
    value
):
    update_softs(
        None,
        db,
        vbox,
        app_box,
        cache,
        results_by_page,
        page,
        pagination_widget,
        value
    )

def match_cb(
    completion,
    model,
    _iter,
    db,
    vbox,
    app_box,
    cache,
    results_by_page,
    page,
    pagination_widget
):
    value = model[_iter][0].strip()
    autocomple_results(
        db,
        vbox,
        app_box,
        cache,
        results_by_page,
        page,
        pagination_widget,
        value
    )

def entry_keypress_cb(
    tab_completion_entry_widget,
    event,
    db,
    vbox,
    app_box,
    cache,
    results_by_page,
    page,
    pagination_widget
):
    entry_completion_widget = tab_completion_entry_widget.get_completion()
    key = Gdk.keyval_name(event.keyval).upper()
    print('KEPRESS', key)
    if key == 'BACKSPACE':
        value = tab_completion_entry_widget.get_text()[:-1]
    else:
        value = tab_completion_entry_widget.get_text() + event.string
    value = value.strip()
    if key in ['RETURN', 'KP_ENTER']:
        autocomple_results(
            db,
            vbox,
            app_box,
            cache,
            results_by_page,
            page,
            pagination_widget,
            value
        )
    liststore = completion_getter(db, value)
    entry_completion_widget.set_model(liststore)
    entry_completion_widget.complete()
    return False

def completion_getter(db, value):
    liststore = Gtk.ListStore(str)
    keywords = db.get_keywords(value)
    for k in keywords:
        liststore.append([k[0]])
    return liststore


class TabCompletionEntry(Gtk.SearchEntry):
    def __init__(
        self,
        db,
        vbox,
        app_box,
        cache,
        results_by_page,
        page,
        pagination_widget
    ):
        Gtk.Entry.__init__(self)
        self.db = db

        self.completion_getter = completion_getter
        self.completing = ''

        entry_completion_widget = Gtk.EntryCompletion()
        entry_completion_widget.set_model(None)
        entry_completion_widget.set_inline_selection(True)
        entry_completion_widget.set_minimum_key_length(2)
        entry_completion_widget.set_text_column(0)

        self.set_completion(entry_completion_widget)
        self.completed = True

        self.connect(
            'key-press-event',
            entry_keypress_cb,
            db,
            vbox,
            app_box,
            cache,
            results_by_page,
            page,
            pagination_widget
        )
        entry_completion_widget.connect(
            'match-selected',
            match_cb,
            db,
            vbox,
            app_box,
            cache,
            results_by_page,
            page,
            pagination_widget
        )
