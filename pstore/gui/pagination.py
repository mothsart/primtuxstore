import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gdk, GdkPixbuf

from ..lib.pagination import pagination

def pages_widget(
    pages,
    hbox,
    db,
    vox,
    app_box,
    cache,
    page,
    results_by_page,
    update_softs,
    value=None
):
    if not pages:
        return
    for p in pages:
        page_b = Gtk.Button.new_with_label(str(p))
        if str(p) == '...' or page == p:
            page_b.set_sensitive(False)
        else:
            page_b.connect(
                'clicked',
                update_softs,
                db,
                vox,
                app_box,
                cache,
                results_by_page,
                p,
                pagination_widget,
                value
            )
        hbox.pack_start(page_b, False, False, True)

def pagination_widget(
    db,
    vbox,
    app_box,
    cache,
    page,
    results_by_page,
    nb_pages,
    update_softs,
    value=None
):
    hbox = Gtk.HBox(homogeneous=False)
    first_b = Gtk.Button.new_with_label("Premier")
    if page == 1:
        first_b.set_sensitive(False)
    else:
        first_b.connect(
            'clicked',
            update_softs,
            db,
            vbox,
            app_box,
            cache,
            results_by_page,
            1,
            pagination_widget,
            value
        )
    hbox.pack_start(first_b, False, False, False)

    before_b = Gtk.Button.new_with_label("Précédent")
    if page == 1:
        before_b.set_sensitive(False)
    else:
        before_b.connect(
            'clicked',
            update_softs,
            db,
            vbox,
            app_box,
            cache,
            results_by_page,
            page - 1,
            pagination_widget,
            value
        )
    hbox.pack_start(before_b, False, False, False)

    pages_widget(
        pagination(page, nb_pages),
        hbox,
        db,
        vbox,
        app_box,
        cache,
        page,
        results_by_page,
        update_softs,
        value
    )

    next_b = Gtk.Button.new_with_label("Suivant")
    if page == nb_pages:
        next_b.set_sensitive(False)
    else:
        next_b.connect(
            'clicked',
            update_softs,
            db,
            vbox,
            app_box,
            cache,
            results_by_page,
            page + 1,
            pagination_widget,
            value
        )
    hbox.pack_start(next_b, False, False, True)

    last_b = Gtk.Button.new_with_label("Dernier")
    if page == nb_pages:
        last_b.set_sensitive(False)
    else:
        last_b.connect(
            'clicked',
            update_softs,
            db,
            vbox,
            app_box,
            cache,
            results_by_page,
            nb_pages,
            pagination_widget,
            value
        )
    hbox.pack_start(last_b, False, False, False)
    hbox.show_all()
    return hbox
