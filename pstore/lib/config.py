from os.path import join

from .debug import is_debug_mode

class DbConf:
    path = 'primtuxstore.db'
    drop_path = join('SQL', 'DROP.sql')
    create_path = join('SQL', 'CREATE.sql')
    
    def __init__(self, debug=False):
        if debug:
            return
        self.path = ''

class Conf:
    sessions = ['super', 'mini', 'maxi', 'prof']
    results_by_page = 10

    def __init__(self, verbose=False):
        """Init Primtux store config"""
        self.verbose = verbose
        self.DEBUG = False
        if is_debug_mode():
            self.DEBUG = True
        self.assets = ''
        self.db = DbConf(self.DEBUG)
