import re
import inspect

import sqlite3


class DataBase:
    def __init__(self, conf):
        self.debug = conf.DEBUG
        self.db = sqlite3.connect(conf.db.path)

    def _where_like(self, value):
        if value:
            return "WHERE k.text LIKE '" + value + "%'"
        return ''

    def _exec(self, request, commit=False):
        if self.debug:
            request = re.sub('\s+', ' ', request).strip()
            previous_frame = inspect.currentframe().f_back
            frame = inspect.getframeinfo(previous_frame)
            print('sql (%s) : %s' % (frame[2], request))
        result = self.db.execute(request)
        if commit:
            self.db.commit()
        return result

    def count_results(self, value=None):
        if value:
            where = " WHERE k.text LIKE '" + value + "%'"
            return self._exec(
                '''
                SELECT count(r.id)
                FROM results AS r
                INNER JOIN contains AS c ON c.id_results = r.id
                INNER JOIN keywords AS k ON k.id = c.id_keywords
                %s;
                ''' % self._where_like(value)
            ).fetchone()[0]
        return self._exec(
            '''
            SELECT count(id) FROM results;
            '''
        ).fetchone()[0]

    def get_page(self, offset, nb, value=None):
        if value:
            return self._exec(
                '''
                SELECT name, app_name, generic, icon_path, path, in_mini, in_maxi, in_super, in_prof
                FROM results AS r
                INNER JOIN contains AS c ON c.id_results = r.id
                INNER JOIN keywords AS k ON k.id = c.id_keywords
                %s
                LIMIT %s, %s;
                ''' % (self._where_like(value), offset, nb)
            )
        return self._exec(
            '''
            SELECT name, app_name, generic, icon_path, path, in_mini, in_maxi, in_super, in_prof
            FROM results LIMIT %s, %s;
            ''' % (offset, nb)
        )

    def app_exist(self, name, sessions, session):
        result = self._exec(
            '''
            SELECT %s, %s, %s, %s
            FROM results WHERE name = "%s";
            ''' % (
                'in_%s' % sessions[0],
                'in_%s' % sessions[1],
                'in_%s' % sessions[2],
                session,
                name
            )
        ).fetchone()
        if not result:
            return None
        return {
            sessions[0]: result[0],
            sessions[1]: result[1],
            sessions[2]: result[2],
            session: result[3]
        }

    def add_app(
        self,
        sessions,
        app_name,
        name,
        icon_path,
        cmd,
        generic,
        session
    ):
        return self._exec(
            '''
            INSERT INTO results
            (name, app_name, generic, icon_path, path, %s, %s, %s, %s)
            VALUES ("%s", "%s", "%s", "%s", '%s', 0, 0, 0, 1);
            ''' % (
                'in_%s' % sessions[0],
                'in_%s' % sessions[1],
                'in_%s' % sessions[2],
                session,
                name,
                app_name,
                generic,
                icon_path,
                cmd
            ),
            True
        ).lastrowid

    def update_app(self, name, session):
        return self._exec(
            '''
            UPDATE results
            set %s = 1
            WHERE name ="%s";
            ''' % (
                session,
                name
            )
        )

    def add_keyword(self, keyword, id_results):
        keywords_cursor = self._exec(
            '''
            SELECT k.id FROM keywords AS k
            WHERE k.text = "%s";
            ''' % keyword
        ).fetchone()
        if keywords_cursor:
            keywords_id = keywords_cursor[0]
        else:
            keywords_id = self._exec(
                '''
                INSERT INTO keywords(text) VALUES("%s");
                ''' % keyword
            ).lastrowid
        self._exec(
            '''
            INSERT INTO contains(id_results, id_keywords)
            VALUES (%s, %s);
            ''' % (id_results, keywords_id)
        )

    def get_keywords(self, value=None):
        return self._exec(
            '''
            SELECT k.text, count(k.id) FROM keywords AS k
            INNER JOIN contains AS c ON k.id = c.id_keywords
            %s
            GROUP BY k.id
            ORDER BY count(k.id) DESC
            LIMIT 20;
            ''' % self._where_like(value)
        )
