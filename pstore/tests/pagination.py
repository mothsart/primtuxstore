from unittest import TestCase

from pstore.lib.pagination import pagination

LAST = 20
RESULTS = [
    [1, 2, 3, '...', LAST],
    [1, 2, 3, 4, '...', LAST],
    [1, 2, 3, 4, 5, '...', LAST],
    [1, 2, 3, 4, 5, 6, '...', LAST],
    [1, 2, 3, 4, 5, 6, 7, '...', LAST],
    [1, '...', 4, 5, 6, 7, 8, '...', LAST],
    [1, '...', 5, 6, 7, 8, 9, '...', LAST],
    [1, '...', 6, 7, 8, 9, 10, '...', LAST],
    [1, '...', 7, 8, 9, 10, 11, '...', LAST],
    [1, '...', 8, 9, 10, 11, 12, '...', LAST],
    [1, '...', 9, 10, 11, 12, 13, '...', LAST],
    [1, '...', 10, 11, 12, 13, 14, '...', LAST],
    [1, '...', 11, 12, 13, 14, 15, '...', LAST],
    [1, '...', 12, 13, 14, 15, 16, '...', LAST],
    [1, '...', 13, 14, 15, 16, 17, '...', LAST],
    [1, '...', 14, 15, 16, 17, 18, 19, LAST],
    [1, '...', 15, 16, 17, 18, 19, LAST],
    [1, '...', 16, 17, 18, 19, LAST],
    [1, '...', 17, 18, 19, LAST]
]

class TestPagination(TestCase):
    def test_results(self):
        last = LAST
        for i in range(1, last):
            assert RESULTS[i - 1] == pagination(i, LAST)
