import os

import apt

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gdk

from .lib.config import Conf
from .lib.request import DataBase

from .gui.autocomplete import TabCompletionEntry
from .gui.pagination import pagination_widget
from .gui.app import update_softs


def gtk_style():
    css = b"""
#apps {
    border: solid grey 1px;
}
    """
    style_provider = Gtk.CssProvider()
    style_provider.load_from_data(css)

    Gtk.StyleContext.add_provider_for_screen(
        Gdk.Screen.get_default(),
        style_provider,
        Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION
    )

class StoreWindow(Gtk.Window):
    def __init__(self, conf, cache):
        Gtk.Window.__init__(
            self,
            title = 'Primtux Store'
        )
        self.set_default_size(1024, 800)

        # db initiate
        db = DataBase(conf)

        app_box = Gtk.Box(homogeneous = True)
        vbox = Gtk.VBox(homogeneous = False)
        
        # autocomplet Widget
        entry_comp = TabCompletionEntry(
            db,
            vbox,
            app_box,
            cache,
            conf.results_by_page,
            1,
            pagination_widget
        )
        vbox.pack_start(entry_comp, True, True, True)
        
        # list of apps
        swin = Gtk.ScrolledWindow()
        swin.set_size_request(1024, 700)
        swin.add(app_box)
        vbox.pack_start(swin, False, False, False);

        update_softs(
            None,
            db,
            vbox,
            app_box,
            cache,
            conf.results_by_page,
            1,
            pagination_widget
        )

        self.add(vbox)
        self.show_all()


def main():
    cache = apt.cache.Cache()
    cache.open()

    gtk_style()

    win = StoreWindow(Conf(), cache)
    win.connect("destroy", Gtk.main_quit)
    win.show_all()
    Gtk.main()
    exit(0)
