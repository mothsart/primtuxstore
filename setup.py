#!/usr/bin/env python3

from setuptools import setup

setup(
    name='primtuxstore',
    version=0.1,
    description=(
        '...'
    ),
    author='mothsart',
    author_email='ferryjeremie@free.fr',
    url='https://framagit.org/mothsart/primtuxstore',
    packages=[ 'pstore' ],
    scripts=['primtuxstore']
)
